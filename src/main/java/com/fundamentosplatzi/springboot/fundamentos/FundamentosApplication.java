package com.fundamentosplatzi.springboot.fundamentos;

import com.fundamentosplatzi.springboot.fundamentos.bean.MyBean;
import com.fundamentosplatzi.springboot.fundamentos.bean.MyBeanWithDependency;
import com.fundamentosplatzi.springboot.fundamentos.bean.MyBeanWithProperties;
import com.fundamentosplatzi.springboot.fundamentos.component.ComponentDependency;
import com.fundamentosplatzi.springboot.fundamentos.entity.User;
import com.fundamentosplatzi.springboot.fundamentos.pojo.UserPojo;
import com.fundamentosplatzi.springboot.fundamentos.repository.UserRepository;
import com.fundamentosplatzi.springboot.fundamentos.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class FundamentosApplication implements CommandLineRunner {

	private ComponentDependency componentDependency;
	private MyBean myBean;
	private MyBeanWithDependency myBeanWithDependency;
	private MyBeanWithProperties myBeanWithProperties;
	private UserPojo userPojo;
	private Log LOGGER = LogFactory.getLog(FundamentosApplication.class);
	private UserRepository userRepository;
	private UserService userService;

	@Autowired
	public FundamentosApplication(@Qualifier("componentTwoImplement") ComponentDependency componentDependency, MyBean myBean, MyBeanWithDependency myBeanWithDependency, MyBeanWithProperties myBeanWithProperties, UserPojo userPojo, UserRepository userRepository, UserService userService){
		this.componentDependency = componentDependency;
		this.myBean = myBean;
		this.myBeanWithDependency = myBeanWithDependency;
		this.myBeanWithProperties = myBeanWithProperties;
		this.userPojo = userPojo;
		this.userRepository = userRepository;
		this.userService = userService;
	}

	public static void main(String[] args) {
		SpringApplication.run(FundamentosApplication.class, args);
	}

	@Override
	public void run(String... args){
		//clasesAnteriores();
		saveUsersInDataBase();
		getInformationJpqlFromUser();
		saveWithErrorTransaction();
	}

	private void saveUsersInDataBase(){
		User user1 = new User("John","john@domain.com", LocalDate.of(2015,03,20));
		User user2 = new User("Maria","maria@domain.com", LocalDate.of(2010,05,21));
		User user3 = new User("Flor","flor@domain.com", LocalDate.of(2014,10,11));
		User user4 = new User("Sandra","sandra@domain.com", LocalDate.of(2013,01,05));
		User user5 = new User("Adriana","adriana@domain.com", LocalDate.of(2002,03,18));
		User user6 = new User("Gildardo","gildardo@domain.com", LocalDate.of(2008,04,19));
		User user7 = new User("Daniel","daniel@domain.com", LocalDate.of(2007,07,27));
		User user8 = new User("John Felipe","felipe@domain.com", LocalDate.of(2009,12,07));
		User user9 = new User("Marina","marina@domain.com", LocalDate.of(2011,11,06));
		User user10 = new User("Yaneth","yaneth@domain.com", LocalDate.of(2012,10,03));
		List<User> list = Arrays.asList(user1,user2,user3,user4,user5,user6,user7,user8,user9,user10);
		list.forEach(userRepository::save);
	}

	private void saveWithErrorTransaction(){
		User test1 = new User("Test1Transactional1","test1Transactional1@dominio.com",LocalDate.now());
		User test2 = new User("Test2Transactional1","test2Transactional1@dominio.com",LocalDate.now());
		User test3 = new User("Test3Transactional1","test3Transactional1@dominio.com",LocalDate.now());
		User test4 = new User("Test4Transactional1","test4Transactional1@dominio.com",LocalDate.now());

		List<User> users = Arrays.asList(test1,test2,test3,test4);

		userService.saveTransactional(users);

		userService.getAllUsers()
				.forEach(user -> LOGGER.info("Este es el usuario dentro del método transaccional: " + user));
	}

	private void getInformationJpqlFromUser(){
		LOGGER.info("El usuario encontrado es: " + userRepository.findByUserEmail("marina@domain.com")
				.orElseThrow(() -> new RuntimeException("No se encontró el usuario")));

		userRepository.findAndSort("Jo", Sort.by("id"))
				.forEach(user -> LOGGER.info("Usuario con método findAndSort: " + user));

		userRepository.findByName("Marina")
				.forEach(user -> LOGGER.info("Usuario con findByName: " + user));

		userRepository.findByNameLike("%J%")
				.forEach(user -> LOGGER.info("Usuario con findByNameLike: " + user));

		userRepository.findByNameOrEmail(null,"gildardo@domain.com")
				.forEach(user -> LOGGER.info("Usuario con findByNameOrEmail: " + user));

		userRepository.findByBirthdateBetween(LocalDate.of(2008, 01, 01),
				LocalDate.of(2012, 12, 30))
				.forEach(user -> LOGGER.info("Usuario con findByBirthdateBetween: " + user));

		userRepository.findByNameLikeOrderByIdDesc("%John%")
				.forEach(user -> LOGGER.info("Usuario con findByNameLikeOrderByIdDesc: " + user));

		userRepository.findByNameContainingOrderByIdAsc("John")
				.forEach(user -> LOGGER.info("Usuario con findByNameLikeOrderByIdDesc: " + user));

		/*LOGGER.info("El usuario a partir del NamedParameter: " + userRepository.findByNameNameQuery("Daniel")
				.orElseThrow(() -> new RuntimeException("no se encontró el usuario con ese nombre")));*/
	}

	private void clasesAnteriores(){
		componentDependency.saludar();
		myBean.print();
		myBeanWithDependency.printWithDependency();
		System.out.println(myBeanWithProperties.function());
		System.out.println(userPojo.getEmail() + "-" + userPojo.getPassword());
		System.out.println(userPojo.getEdad());
		try{
			int value = 10/0;
			LOGGER.info("Mi valor es " + value);
		}catch(Exception e) {
			LOGGER.error("Esto es un error al dividir por cero " + e.getMessage());
		}
	}
}
