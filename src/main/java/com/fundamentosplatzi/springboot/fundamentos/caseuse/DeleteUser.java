package com.fundamentosplatzi.springboot.fundamentos.caseuse;

import com.fundamentosplatzi.springboot.fundamentos.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class DeleteUser {
    public DeleteUser(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    public void remove(Long id) {
        userService.delete(id);
    }
}
